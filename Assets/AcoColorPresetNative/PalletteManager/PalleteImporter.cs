﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.IO;


public class PalletteImporter : AssetPostprocessor
{
	static string extension = ".aco";        
	static string newExtension = ".asset"; 
	
	public static bool HasExtension(string asset)
	{
		return asset.EndsWith(extension, System.StringComparison.OrdinalIgnoreCase);
	}
	
	public static string ConvertToInternalPath(string asset)
	{
		string left = asset.Substring(0, asset.Length - extension.Length);
		return left + newExtension;
	}
	
	// This is called always when importing something
	static void OnPostprocessAllAssets
		(
			string[] importedAssets,
			string[] deletedAssets,
			string[] movedAssets,
			string[] movedFromAssetPaths
			)
	{
		foreach(string asset in importedAssets)
		{
			if(HasExtension(asset))
			{
			//	Debug.Log ("Found file");

				Debug.Log (asset);
				string newPath = ConvertToInternalPath(asset);

				Pallette pallette = AssetDatabase.LoadAssetAtPath(newPath, typeof(Pallette)) as Pallette;
				bool loaded = (pallette != null);
				
				
				
				if(!loaded)
				{
					pallette = ScriptableObject.CreateInstance<Pallette>();
				}
		
				

				using (BinaryReader b = new BinaryReader(File.Open(Application.dataPath +"/../"+ asset, FileMode.Open)))
				{
					// 2.
					// Position and length variables.
					int pos = 0;
					// 2A.
					// Use BaseStream.
					int length = (int)b.BaseStream.Length;

					byte[] chunk;

					AssetDatabase.SaveAssets();

					UInt16 version = ByteToWord(b.ReadBytes(2));

					//Debug.Log ("pallette format version: " + version);
					UInt16 number_of_colors = ByteToWord(b.ReadBytes(2));
				//	Debug.Log ("number of colours: " + number_of_colors);
					for (UInt16 i = 0 ; i <number_of_colors; i++)
					{
						
						UInt16 colorSpace = ByteToWord(b.ReadBytes(2));
						//Debug.Log ("v1 - Color Space " + colorSpace);
						UInt16 red = ByteToWord(b.ReadBytes(2));
					  //Debug.Log ("v1 -red " + red);
						
						UInt16 green = ByteToWord(b.ReadBytes(2));
						//Debug.Log ("v1 - green " + green);
						
						UInt16 blue = ByteToWord(b.ReadBytes(2));
						//Debug.Log ("v1 - blue " + blue);
						
		
						
						UInt16 end =  ByteToWord(b.ReadBytes(2));


						pos+= sizeof(byte)*(10);

					}	
					//Debug.Log ("version 2: "+  ByteToWord(b.ReadBytes(2)));
					//Debug.Log ("version 2 colours : "+  ByteToWord(b.ReadBytes(2)));

					pos += sizeof(byte)*8 ;
					while (pos < length)
					{
				
						UInt16 colorSpace = ByteToWord(b.ReadBytes(2));
						//Debug.Log ("Color Space " + colorSpace);

						if (colorSpace == 0)
						{
							pos += ProcessRGB(b,ref pallette);
						}
					
						else if (colorSpace == 1)
						{
							pos += ProcessHSB(b,ref pallette);
						}

					}
				}
			
				if(!loaded)
				{
					AssetDatabase.CreateAsset(pallette,newPath);
				}
			}
		}

	
	}


	static int ProcessRGB (BinaryReader b, ref Pallette pallette)
	{
		UInt16 red = ByteToWord(b.ReadBytes(2));
		//Debug.Log ("red " + red);
		
		UInt16 green = ByteToWord(b.ReadBytes(2));
		//Debug.Log ("green " + green);
		
		UInt16 blue = ByteToWord(b.ReadBytes(2));
		//Debug.Log ("blue " + blue);
		
		UInt16 z = ByteToWord(b.ReadBytes(2));
		//Debug.Log ("z " + z);
		
		UInt16 end =  ByteToWord(b.ReadBytes(2));
		
		
		UInt16 title_length = (UInt16)ByteToWord(b.ReadBytes(2));
		
		//Debug.Log ("title length: " + title_length);
		
		char[] name = new char [title_length-1];
		
		for (UInt16 i = 1; i < title_length;i++)
		{
			name[i-1] =  Convert.ToChar( ByteToWord(b.ReadBytes(2)));
		}
		
		string title = new string(name);
		
		end =  ByteToWord(b.ReadBytes(2));
		
		
		Color temp = new Color ((float)red/65535.0f,
		                        (float)green/65535.0f,
		                        (float)blue/65535.0f);
		//Debug.Log ("Color ---  " + title+" ----" + temp);
		

		pallette.listOfNames.Add(title);
		pallette.listOfColors.Add(temp);
		return sizeof(byte)* (14 + title_length*2);

	}
	
	static int ProcessHSB (BinaryReader b, ref Pallette pallette)
	{
		UInt16 H = ByteToWord(b.ReadBytes(2));
		//Debug.Log ("H " + H);
		
		UInt16 S = ByteToWord(b.ReadBytes(2));
		//Debug.Log ("S " + S);
		
		UInt16 B = ByteToWord(b.ReadBytes(2));
		//Debug.Log ("B " + B);
		
		UInt16 z = ByteToWord(b.ReadBytes(2));
		//Debug.Log ("z " + z);
		
		UInt16 end =  ByteToWord(b.ReadBytes(2));
		
		
		UInt16 title_length = (UInt16)ByteToWord(b.ReadBytes(2));
		
		//Debug.Log ("title length: " + title_length);
		
		char[] name = new char [title_length-1];
		
		for (UInt16 i = 1; i < title_length;i++)
		{
			name[i-1] =  Convert.ToChar( ByteToWord(b.ReadBytes(2)));
		}
		
		string title = new string(name);
		
		end =  ByteToWord(b.ReadBytes(2));
		
		
		Color temp = HSVTORGB((float)H/182.04f,(float)S/65535f,(float)B/65535f);

		//Debug.Log ("Color ---  " + title+" ----" + temp);
		
		
		pallette.listOfNames.Add(title);
		pallette.listOfColors.Add(temp);
		return sizeof(byte)* (14 + title_length*2);
		
	}

	static Color HSVTORGB(float H, float S, float V)
	{
		float C = V * S;

		float X = C * (1 -Mathf.Abs( ((H/60)%2)-1 ));
		float M = V-C;

		if (H < 60)
		{

			float R = (C+M);
			float G = (X+M);
			float B = (0+M);

			return new Color (R,G,B);
		}

		else if (H >= 60 && H < 120)
		{
			float R = (X+M) ;
			float G = (C+M);
			float B = (0+M) ;
			
			return new Color (R,G,B);
		}

		else if (H >= 120 && H < 180)
		{
			float R = (0+M) ;
			float G = (C+M) ;
			float B = (X+M);
			
			return new Color (R,G,B);
		}

		else if (H >= 180 && H < 240)
		{
			float R = (0+M) ;
			float G = (X+M);
			float B = (C+M) ;
			
			return new Color (R,G,B);
		}

		else if (H >= 240 && H < 300)
		{
			float R = (X+M) ;
			float G = (0+M) ;
			float B = (C+M) ;
			
			return new Color (R,G,B);
		}

		else if (H >= 300 && H <= 360)
		{
			float R = (C+M) ;
			float G = (0+M) ;
			float B = (X+M) ;
			
			return new Color (R,G,B);
		}
		

		return Color.black;

		




	}


	static UInt16 ByteToWord (byte[] chunk)
	{
		return (UInt16) (chunk[1] | (chunk[0] << 8));
	}
	// Imports my asset from the file

}