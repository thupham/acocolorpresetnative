﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Pallette : ScriptableObject {


	/// <summary>
	/// The list of names. Don't reference these directly, should be private.
	/// </summary>
	public List<string> listOfNames = new List<string>();
	/// <summary>
	/// The list of colours. Don't reference these directly, should be private.
	/// </summary>
	public List<Color> listOfColors = new List<Color>();
	 
	public Color RandomColour ()
	{
		Color c = listOfColors[Random.Range(0,listOfColors.Count)];
		return ( QualitySettings.activeColorSpace == ColorSpace.Linear)?c.linear : c;
	}

	/// <summary>
	/// Gets the colour by index.
	/// </summary>
	/// <returns>The colour.</returns>
	/// <param name="n">N.</param>
	public Color GetColour (int n)
	{
		Color c = listOfColors[n];
		return ( QualitySettings.activeColorSpace == ColorSpace.Linear)? c.linear : c;
	}

	/// <summary>
	/// Gets the colour name.
	/// </summary>
	/// <returns>The colour.</returns>
	/// <param name="s">S.</param>
	public Color GetColour (string s)
	{
		Color c = listOfColors[listOfNames.IndexOf(s)];
		return ( QualitySettings.activeColorSpace == ColorSpace.Linear)?c.linear : c;
	}

}
