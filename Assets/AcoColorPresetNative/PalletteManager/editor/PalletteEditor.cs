﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.IO;
using UnityEditor;
using System.Collections.Generic;


[CustomEditor(typeof(Pallette))]
public class PalletteEditor : Editor 
{
	Pallette myTarget;
	
	public override void OnInspectorGUI ()
	{
		if (myTarget == null)
			myTarget = (Pallette)target;
	
		EditorGUILayout.LabelField("Colour Palette");
		for (int i = 0 ; i < myTarget.listOfNames.Count;i ++)
		{
			EditorGUILayout.BeginHorizontal();
			myTarget.listOfNames[i] = EditorGUILayout.TextField(myTarget.listOfNames[i]);
			myTarget.listOfColors[i] = EditorGUILayout.ColorField(myTarget.listOfColors[i]);
			if (GUILayout.Button("X",GUILayout.Width(25)))
			  {
				myTarget.listOfNames.RemoveAt(i);
				myTarget.listOfColors.RemoveAt(i);

				}
			EditorGUILayout.EndHorizontal();
		}

		if (GUILayout.Button("Add Colour"))
		{
			myTarget.listOfNames.Add("New color");
			myTarget.listOfColors.Add (Color.white);
		}
    if (GUILayout.Button ("Convert to native")) {
      var colorPresetLibraryType = Type.GetType("UnityEditor.ColorPresetLibrary, UnityEditor");
      var library = ScriptableObject.CreateInstance(colorPresetLibraryType);

      int count = 0;
      foreach (Color c in myTarget.listOfColors) {
        addColor (library, c, myTarget.listOfNames[count]);
        count++;
      }

      AssetDatabase.CreateAsset(library, "Assets/Editor/"+myTarget.name+".colors");
      AssetDatabase.SaveAssets();
      AssetDatabase.Refresh();
    }
	}

  static void addColor(object library, Color color, string name)
  {
    var curvePresetLibraryType = Type.GetType("UnityEditor.ColorPresetLibrary, UnityEditor");
    var addMethod = curvePresetLibraryType.GetMethod("Add");
    addMethod.Invoke(library, new object[]
      {
        color,
        name
      });
  }
	
}